// ignore_for_file: prefer_const_constructors

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:my_tv/API/models.dart';
import 'dart:async';

import '../UI/screens.dart';
import 'controllers.dart';

class ShowWidget extends StatelessWidget {
  final ShowModel model;

  ShowWidget(this.model);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double len = min(size.width,size.height);
    return GestureDetector(
      child: Container(
          width: len*0.47,
          height: len*0.75,
          margin: EdgeInsets.all(len*0.015),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(len*0.05),
            color: Colors.grey
          ),

        child: LayoutBuilder(
          builder: (context,constrains) {
            return Column(
              children: [
                Expanded(
                    child: Container(
                      width: double.infinity,
                      height: double.infinity,

                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.vertical(top: Radius.circular(len*0.05)),
                        image: DecorationImage(
                          image: NetworkImage(model.image),
                          fit: BoxFit.fill
                        )
                      ),
                    )
                ),
                ListTile(
                  title: Text(model.title),
                  subtitle: Row(

                    children: [
                      Icon(Icons.star,color: Colors.amber,),
                      SizedBox(width: constrains.maxWidth*0.035,),
                      Text('${model.imDbRating}/10'),
                    ],
                  ),

                  trailing: Text(model.rank),
                ),
              ],
            );
          }
        ),

      ),
      onTap: (){
        Navigator.push(context, MaterialPageRoute(
            builder: (c){
              return ShowScreen(model);
            }
        )
        );
      },
    );
  }

  static Future<List<ShowWidget>> getTop250TVsWidgets() async{
    List<ShowModel> models = await ShowController.getTop250TVs();
    List<ShowWidget> widgets = [];

    for(var x in models)
      widgets.add(ShowWidget(x));

    return widgets;
  }

  static Future<List<ShowWidget>> getTop250MoviesWidgets() async{
    List<ShowModel> models = await ShowController.getTop250movies();
    List<ShowWidget> widgets = [];

    for(var x in models)
      widgets.add(ShowWidget(x));

    return widgets;
  }

}

class SearchWidget extends StatelessWidget {
  final SearchModel model;

  SearchWidget(this.model);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text(model.title),
        subtitle: Text(model.description),
        leading: CircleAvatar(
          foregroundImage: NetworkImage(model.image),
        ),
      ),
    );
  }

  static Future<List<SearchWidget>> search(String value) async{
    List<SearchModel> results = await ShowController.search(value);
    List<SearchWidget> widgets = [];

    for(SearchModel x in results){
      widgets.add(SearchWidget(x));
    }
    return widgets;
  }
}



