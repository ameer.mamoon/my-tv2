import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'models.dart';

class ShowController{


  static Future<List<ShowModel>> _getTop250(String url) async{
    var response = await http.get(Uri.parse(url));
    Map<String,dynamic> data = jsonDecode(response.body);

    List<dynamic> items = data['items'];
    List<ShowModel> models = [];

    for(Map<String,dynamic> x in items){
      models.add(ShowModel.map(x));
    }

    print(models.length);
    return models;
  }

  static Future<List<ShowModel>> getTop250TVs() async{
    return await _getTop250('https://jsonkeeper.com/b/FS9A');
  }

  static Future<List<ShowModel>> getTop250movies() async{
    return await _getTop250('https://jsonkeeper.com/b/TE8N');
  }

  static Future<List<SearchModel>> search(String value)async{
    var response = await http.get(Uri.parse('https://imdb-api.com/en/API/SearchTitle/k_2k6w9uxj/$value'));
    Map<String,dynamic> data = jsonDecode(response.body);

    List<dynamic> results = data['results'];
    List<SearchModel> models = [];

    for(Map<String,dynamic> x in results) {
      models.add(SearchModel.map(x));
    }

    return models;
  }



}

