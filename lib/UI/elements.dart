// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:get/get.dart';


class SearchController extends GetxController{
  bool isSearch = false;

  void press(){
    isSearch = !isSearch;
    update();
  }
}


class SearchBox extends StatelessWidget {
  final String title;
  final SearchController controller;
  final TextEditingController textController;

  SearchBox(this.title,this.controller,this.textController);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SearchController>(
      init: controller ,
      builder: (controller){
        return
            controller.isSearch ?
            TextField(
              controller: textController,
              decoration: InputDecoration(
                hintText: 'Search a movie or a TV show',
                icon: IconButton(
                  icon:Icon(Icons.close,color: Colors.white,),
                  onPressed: (){
                    textController.text = '';
                    controller.press();
                  },
                )
              ),
            ):
            Text(title);
      },
    );
  }
}
