// ignore_for_file: prefer_const_constructors

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_tv/API/models.dart';
import 'package:my_tv/UI/elements.dart';
import '../API/view.dart';

class Home extends StatelessWidget {

  final Future future;
  final String title;

  static final SearchController searchController = SearchController();
  static final TextEditingController textController = TextEditingController();

  Home(this.future,this.title);

  @override
  Widget build(BuildContext context) {

    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: SearchBox('Home : $title',searchController,textController),
        centerTitle: true,
        actions: [
          IconButton(
              onPressed: (){
                searchController.press();
                String value = textController.text;
                textController.text = '';
                if(value.isNotEmpty) {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context){
                    return SearchResultScreen(value);
                  })
                  );
                }

              },
              icon: Icon(Icons.search))
        ],
      ),
      body: FutureBuilder(
        future: future,
        builder: (context,snapShot){

          if(snapShot.hasData){
            return SingleChildScrollView(
              child: Wrap(
                children: snapShot.data as List<Widget>,
              ),
            );
          }

          if(snapShot.hasError) {
            return  Text(snapShot.error.toString());
          }

          return const Center(child:  CircularProgressIndicator());
        },
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            Container(
              width: double.infinity,
              color: Theme.of(context).primaryColor,
              height: size.height*0.25,
              padding: EdgeInsets.all(size.width*0.075),
              child: FittedBox(
                child: Row(
                  children: [
                    Icon(Icons.play_circle_fill),
                    SizedBox(
                      width: size.width*0.01,
                    ),
                    Text('My Movies')
                  ],
                ),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Top TVs'),
                subtitle: Text('Top 250 TVs in the world'),
                leading: Icon(Icons.tv),
                onTap: (){
                  Navigator.pushReplacementNamed(context, 'tvScreen');
                },
              ),
            ),
            Card(
              child: ListTile( 
                title: Text('Top Movies'),
                subtitle: Text('Top 250 Movies in the world'),
                leading: Icon(CupertinoIcons.videocam_circle),
                onTap: (){
                  Navigator.pushReplacementNamed(context, 'movieScreen');
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ShowScreen extends StatelessWidget {

  final ShowModel model;


  ShowScreen(this.model);

  @override
  Widget build(BuildContext context) {
    //Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text(model.fullTitle),
        centerTitle: true,
      ),

      body: Column(
        children: [
          Expanded(
              flex: 2,
              child: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(model.image),
                    fit: BoxFit.fill,
                  )
                ),
              )
          ),
          Expanded(
              child: LayoutBuilder(
                builder: (context,constrains){
                  return Container(
                    padding: EdgeInsets.all(constrains.maxWidth*0.05),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text('Crew : ${model.crew}'),
                        Text('Year : ${model.year}'),
                        Text('Global Rank : ${model.rank}'),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.star,color: Colors.amber),
                            Text('Rate : ${model.imDbRating}/10'),
                          ],
                        ),
                        Text('Rating Count : ${model.imDbRatingCount}'),

                      ],
                    ),
                  );
                },
              )
          ),
        ],
      ),

    );
  }
}


class SearchResultScreen extends StatelessWidget {

  final String value;


  SearchResultScreen(this.value);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(value),
      ),
      body: FutureBuilder(
        future: SearchWidget.search(value),
        builder: (context,snapShot){
          if(snapShot.hasData){
            List<Widget> children = snapShot.data as List<Widget>;
            if(children.isEmpty){
              return Center(
                child: Text('No Results!!'),
              );
            }
            return ListView(
              children: children,
            );
          }
          if(snapShot.hasError) {
            return  Text(snapShot.error.toString());
          }

          return const Center(child:  CircularProgressIndicator());
        },
      ),
    );
  }
}
