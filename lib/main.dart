import 'package:flutter/material.dart';
import 'UI/screens.dart';

import 'API/view.dart';





void main(){

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        'tvScreen': (context) => Home(ShowWidget.getTop250TVsWidgets(),'Top TVs'),
        'movieScreen': (context) => Home(ShowWidget.getTop250MoviesWidgets(),'Top Movies'),
      },

      initialRoute: 'tvScreen',
    );
  }
}
